# Small Chip8 Emulator
This is a small, incomplete Chip8 emulator written in C, with SDL as the graphical library.

Some instructions are missing, but most of them are written. Currently, `pong.c8` and `pong2.c8` are fully playable. There are maybe some errors in the interpretation of the emulator, I'll fix them one day.

If you want to start with writing emulators, Chip8 is a good way to start because it is very simple, but you can learn about the fundamentals of how CPUs works, and how emulation by interpretation works.

If you want to build it, you'll need to setup SDL2 with your project.

Some Chip8 ROMs are located in `roms/`.