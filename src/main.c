#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "chip.h"

SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;
int sizeOfPixels = 20;
int quit = 0;
unsigned int keyBuffer[16];

int stop() {
    quit = 1;
    if(NULL != renderer)
        SDL_DestroyRenderer(renderer);
    if(NULL != window)
        SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

void setRendererColor(SDL_Renderer *renderer, SDL_Color color) {
    if(0 != SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a))
    {
        fprintf(stderr, "Erreur SDL_SetRenderDrawColor : %s", SDL_GetError());
        stop();
    }
}

void draw(unsigned char display[]) {
    SDL_Color black = {0, 0, 0, 255};
    SDL_Color white = {245, 245, 245, 255};

    int i = 0;
    int wIndex = 0;
    int bIndex = 0;
    SDL_Rect whiteRects[2048];
    SDL_Rect blackRects[2048];
    for(i = 0; i < 2048; i++)
    {
        if (display[i] != 0) {
            SDL_Rect rect;
            rect.w = sizeOfPixels;
            rect.h = sizeOfPixels;
            rect.x = (i*sizeOfPixels)%(64*sizeOfPixels);
            rect.y = (i*sizeOfPixels/(64*sizeOfPixels))*sizeOfPixels;
            whiteRects[wIndex++] = rect;
            //printf("X");
        } else {
            SDL_Rect rect;
            rect.w = sizeOfPixels;
            rect.h = sizeOfPixels;
            rect.x = (i*sizeOfPixels)%(64*sizeOfPixels);
            rect.y = (i*sizeOfPixels/(64*sizeOfPixels))*sizeOfPixels;
            blackRects[bIndex++] = rect;
            //printf(".");
        }

        if ((i+1) % 64 == 0) {
            //printf("\n");
        }
    }
    //printf("\n\n\n");
    setRendererColor(renderer, white);
    SDL_RenderFillRects(renderer, whiteRects, wIndex+1); // Draw all rectangles at the same time, probably better for performance
    setRendererColor(renderer, black);
    SDL_RenderFillRects(renderer, blackRects, bIndex+1); // Draw all rectangles at the same time, probably better for performance
    SDL_RenderPresent(renderer);
}

int main(int argc, char *argv[])
{
    int delay = 16;
    char fileToLoad[32];
    if (argc == 1) {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
            "Please specify ROM to load",
            "You did not specify a ROM to load, you need to run the program with the ROM name in first argument.",
            NULL);
        exit(EXIT_FAILURE);
    } else if (argc == 2) {
        strcpy(fileToLoad, argv[1]);
        if (access(fileToLoad, F_OK) == -1) {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                "File not found",
                "The ROM file you provided was not found.",
                NULL);
            exit(EXIT_FAILURE);
        }
    } else if (argc == 3) {
        strcpy(fileToLoad, argv[1]);
        sizeOfPixels = atoi(argv[2]);
        printf("New size of pixels : %d\n", sizeOfPixels);
    } else if (argc == 4) {
        strcpy(fileToLoad, argv[1]);
        sizeOfPixels = atoi(argv[2]);
        printf("New size of pixels : %d\n", sizeOfPixels);
        delay = atoi(argv[3]);
        printf("New delay : %d\n", delay);
    }

    /* Initialisation, création de la fenêtre et du renderer. */
    if(0 != SDL_Init(SDL_INIT_VIDEO))
    {
        fprintf(stderr, "Erreur SDL_Init : %s", SDL_GetError());
        stop();
    }
    window = SDL_CreateWindow("Chip 8 Emulator", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              64*sizeOfPixels, 32*sizeOfPixels, SDL_WINDOW_SHOWN);
    if(NULL == window)
    {
        fprintf(stderr, "Erreur SDL_CreateWindow : %s", SDL_GetError());
        stop();
    }
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if(NULL == renderer)
    {
        fprintf(stderr, "Erreur SDL_CreateRenderer : %s", SDL_GetError());
        stop();
    }
    
    if(0 != SDL_RenderClear(renderer))
    {
        fprintf(stderr, "Erreur SDL_SetRenderDrawColor : %s", SDL_GetError());
        stop();
    }

    /*const SDL_MessageBoxButtonData buttons[] = {
        { 0, 0, "no" },
        { SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 1, "yes" },
        { SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 2, "cancel" },
    };
    const SDL_MessageBoxColorScheme colorScheme = {
        { // .colors (.r, .g, .b) 
            // [SDL_MESSAGEBOX_COLOR_BACKGROUND]
            { 255,   0,   0 },
            // [SDL_MESSAGEBOX_COLOR_TEXT]
            {   0, 255,   0 },
            // [SDL_MESSAGEBOX_COLOR_BUTTON_BORDER]
            { 255, 255,   0 },
            // [SDL_MESSAGEBOX_COLOR_BUTTON_BACKGROUND]
            {   0,   0, 255 },
            // [SDL_MESSAGEBOX_COLOR_BUTTON_SELECTED]
            { 255,   0, 255 }
        }
    };
    const SDL_MessageBoxData messageboxdata = {
        SDL_MESSAGEBOX_INFORMATION, // .flags 
        NULL, // .window 
        "example message box", // .title 
        "select a button", // .message 
        SDL_arraysize(buttons), // .numbuttons 
        buttons, // .buttons 
        &colorScheme // .colorScheme 
    };
    int buttonid;
    if (SDL_ShowMessageBox(&messageboxdata, &buttonid) < 0) {
        SDL_Log("error displaying message box");
        return 1;
    }
    if (buttonid == -1) {
        SDL_Log("no selection");
    } else {
        SDL_Log("selection was %s", buttons[buttonid].text);
    }*/

    init();
    printMemory();
    printf("Loading %s\n", fileToLoad);
    loadProgram(fileToLoad);

    // Using helper tab for key input
    unsigned int keyIdToKey[256];
    for(int i = 0; i < 256; i++) {
        keyIdToKey[i] = -1;
    }
    keyIdToKey['1'] = 1;
    keyIdToKey['2'] = 2;
    keyIdToKey['3'] = 3;		
    keyIdToKey['a'] = 4;
    keyIdToKey['z'] = 5;
    keyIdToKey['e'] = 6;
    keyIdToKey['q'] = 7;
    keyIdToKey['s'] = 8;
    keyIdToKey['d'] = 9;
    keyIdToKey['w'] = 0xA;
    keyIdToKey['x'] = 0;
    keyIdToKey['c'] = 0xB;
    keyIdToKey['4'] = 0xC;
    keyIdToKey['r'] = 0xD;
    keyIdToKey['f'] = 0xE;
    keyIdToKey['v'] = 0xF;

    printf("Program read, starting interpretation...\n");
    Uint32 before = SDL_GetTicks();
    
    while (!quit)
    {
        before = SDL_GetTicks();
        SDL_Event event;

        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    quit = 1;
                    break;
                case SDL_KEYDOWN:
                    if(keyIdToKey[event.key.keysym.sym] != -1) {
                        keyBuffer[keyIdToKey[event.key.keysym.sym]] = 1;
                    }
                    break;
                case SDL_KEYUP:
                    if(keyIdToKey[event.key.keysym.sym] != -1) {
                        keyBuffer[keyIdToKey[event.key.keysym.sym]] = 0;
                    }
                    break;
            }
        }

        chipRun();

        if (needsRedraw()) { // Will not draw if not necessary, to ease the GPU
            draw(getDisplay());
            removeDrawFlag();
        }

        Uint32 after = SDL_GetTicks(); // This will make sure the game works at fixed speed
        int realDelay = delay - (after - before);
        if (realDelay > 0) {
            SDL_Delay(realDelay);
        }
    }

    stop();
    return 0;
}

unsigned int *getKeyBuffer() {
    return keyBuffer;
}