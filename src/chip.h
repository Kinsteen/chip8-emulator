#ifndef CHIP_H
#define CHIP_H

void init();
void chipRun();
int needsRedraw();
void removeDrawFlag();
unsigned char *getDisplay();
void loadFontset();
void loadProgram(char *fileName);
void printMemory();

#endif