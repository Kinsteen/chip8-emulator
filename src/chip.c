#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "chip.h"
#include "main.h"

unsigned char memory[4096];
unsigned int V[16];
unsigned int I;
unsigned int pc;

unsigned int stack[16];
unsigned int stackPointer;

unsigned int delay_timer;
unsigned int sound_timer;

unsigned char keys[16];
unsigned char display[64*32];

int redraw;


unsigned int fontset[] = {
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

void init() {
    I = 0x0;
    pc = 0x200;
    stackPointer = 0;

    delay_timer = 0;
    sound_timer = 0;
    redraw = 0;

    loadFontset();
    srand(time(NULL));   // Initialization, should only be called once.
}

void chipRun() {
    unsigned int opcode = (memory[pc]<<8) | memory[pc+1];

    switch(opcode & 0xF000) {
        case 0x0000: // Multi case
            switch (opcode & 0x00FF) {
            case 0x00E0: // Clear screen
                for(int i = 0; i < sizeof(display)/sizeof(display[0]); i++) {
                    display[i] = 0;
                }
                redraw = 1;
                pc += 2;
                break;

            case 0x00EE: // Returns from subroutine
                stackPointer--;
                pc = stack[stackPointer] + 2;
                // printf("Returning to %x\n", pc);
                break;

            default:
                fprintf(stderr, "%x: Opcode not implemented.\n", opcode);
                stop();
                break;
            }
            break;

        case 0x1000: {//1NNN: Jumps to address NNN
            int nnn = opcode & 0x0FFF;
            pc = nnn;
            // printf("Jumping to %x\n", pc);
            break;
        }

        case 0x2000: //2NNN: Calls subroutine at NNN
            stack[stackPointer] = pc;
            stackPointer++;
            pc = opcode & 0x0FFF;
            // printf("Calling %x\n", pc);
            break;

        case 0x3000: { //3XNN: Skips the next instruction if VX equals NN
            int x = (opcode & 0x0F00) >> 8;
            int nn = opcode & 0x00FF;

            if (V[x] == nn) {
                pc += 4;
                // printf("Skipping next instruction\n");
            }
            else {
                pc += 2;
                // printf("Not skipping next instruction\n");
            }
            break;
        }

        case 0x4000: { //3XNN: Skips the next instruction if VX equals NN
            int x = (opcode & 0x0F00) >> 8;
            int nn = opcode & 0x00FF;

            if (V[x] != nn) {
                pc += 4;
                // printf("Skip\n");
            }
            else {
                pc += 2;
                // printf("Not skip \n");
            }
            break;
        }

        case 0x5000: { //3XNN: Skips the next instruction if VX equals NN
            int x = (opcode & 0x0F00) >> 8;
			int y = (opcode & 0x00F0) >> 4;
			
			if (V[x] == V[y]) {
				pc += 4;
				//printf("Skipping next instruction (V[%d] == V[%d])", x, y);
			}
			else {
				pc += 2;
				//printf("Not skipping next instruction (V[%d] != V[%d])", x, y);
			}
			break;
		}

        case 0x6000: { //6XNN: Set VX to NN
            int x = (opcode & 0x0F00) >> 8;
            V[x] = (opcode & 0x00FF);
            pc += 2;
            // printf("Setting V[%x] to %d\n", x, V[x]);
            break;
        }

        case 0x7000: { //7XNN: Adds NN to VX
            int x = (opcode & 0x0F00) >> 8;
            int nn = opcode & 0x00FF;
            V[x] = (V[x] + nn);
            pc += 2;
            // printf("Adding %d to V[%d] = %d\n", nn, x, V[x]);
            break;
        }

        case 0x8000: { //Contains more data in last nibble
            switch(opcode & 0x000F) {
                case 0x0000: { //8XY0: Sets VX to VY
                    int x = (opcode & 0x0F00) >> 8;
                    int y = (opcode & 0x00F0) >> 4;

                    V[x] = V[y];
                    pc += 2;
                    // printf("Set Vx to vy\n");
                    break;
                }

                case 0x0002: { //8XY2: Sets VX to VX & VY
                    int x = (opcode & 0x0F00) >> 8;
                    int y = (opcode & 0x00F0) >> 4;

                    V[x] = (V[x] & V[y]);
                    pc += 2;
                    // printf("Set Vx to vx&vy\n");
                    break;
                }

                case 0x0003: {
                    int x = (opcode & 0x0F00) >> 8;
                    int y = (opcode & 0x00F0) >> 4;
                    
                    V[x] = V[x] ^ V[y];
                    
                    pc += 2;
                    break;
                }

                case 0x0004: { //8XY4: Adds Vy to Vx, with carry
                    int x = (opcode & 0x0F00) >> 8;
                    int y = (opcode & 0x00F0) >> 4;

                    if (V[y] > 0xFF - V[x])
                        V[0xF] = 1;
                    else
                        V[0xF] = 0;

                    V[x] = ((V[x] + V[y]) & 0xFF);
                    pc += 2;
                    break;
                }

                case 0x0005: { //8XY4: Substract Vy to Vx, with borrow
                    int x = (opcode & 0x0F00) >> 8;
                    int y = (opcode & 0x00F0) >> 4;

                    if (V[x] > V[y])
                        V[0xF] = 1;
                    else
                        V[0xF] = 0;

                    V[x] = ((V[x] - V[y]) & 0xFF);
                    pc += 2;
                    break;
                }

                case 0x0006: {
                    int x = (opcode & 0x0F00) >> 8;
                    
                    V[0xF] = V[x] & 0x0001;
                    V[x] = V[x] >> 1;
                    
                    pc += 2;
                    break;
                }

                case 0x000E: {
                    int x = (opcode & 0x0F00) >> 8;
                    
                    V[0xF] = V[x] & 0x8000;
                    V[x] = V[x] << 1;
                    
                    pc += 2;
                    break;
                }

                default:
                    fprintf(stderr, "%x: Opcode not implemented.\n", opcode);
                    stop();
                    break;
            }

            break;
        }

        case 0xA000: //ANNN: Set I to NNN
            I = (opcode & 0x0FFF);
            pc += 2;
            // printf("Set I to %x\n", I);
            break;

        case 0xC000: { //CXNN: Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
            int x = (opcode & 0x0F00) >> 8;
            int nn = (opcode & 0x00FF);
            int r = rand();      // Returns a pseudo-random integer between 0 and RAND_MAX.
            int randomNumber = (r%256) & nn;
            V[x] = (char) randomNumber;
            // printf("V[x] has been set to (randomised) %d\n",randomNumber);
            pc += 2;
            break;
        }

        case 0xD000: { //DXYN: Draw a sprite (X, Y) size (8, N). Sprite is located at I
            int x = V[(opcode & 0x0F00) >> 8];
            int y = V[(opcode & 0x00F0) >> 4];
            int height = opcode & 0x000F;

            V[0xF] = 0;

            for (int _y = 0; _y < height; _y++) {
                int line = memory[I + _y];
                for (int _x = 0; _x < 8; _x++) {
                    int pixel = line & (0x80 >> _x);
                    if (pixel != 0) {
                        int totalX = x + _x;
                        int totalY = y+ _y;

                        totalX = totalX % 64;
                        totalY = totalY % 32;
                        int index = totalY * 64 + totalX;

                        if (display[index] == 1)
                            V[0xF] = 1;

                        display[index] ^= 1;
                    }
                }
            }
            pc += 2;
            redraw = 1;
            // printf("Drawing at V[%d] = %d, V[%d] = %d\n",((opcode & 0x0F00) >> 8), x, ((opcode & 0x00F0) >> 4) , y);
            break;
        }

        case 0xE000: {
            switch (opcode & 0x00FF) {
                case 0x009E: {
                    int x = (opcode & 0x0F00) >> 8;
                    int key = V[x];

                    if (getKeyBuffer()[key] == 1) {
                        pc += 4;
                    } else {
                        pc += 2;
                    }
                    // printf("Skipping next instruction if V[%d] is pressed\n", V[x]);
                    break;
                }


                case 0x00A1: {
                    int x = (opcode & 0x0F00) >> 8;
                    int key = V[x];

                    if (getKeyBuffer()[key] == 0) {
                        pc += 4;
                    } else {
                        pc += 2;
                    }
                    // printf("Skipping next instruction if V[%d] is NOT pressed\n", V[x]);
                    break;
                }

                default:
                    fprintf(stderr, "Unexisting Opcode!\n");
                    stop();
                    break;
                }

            break;
        }


        case 0xF000:
            switch (opcode & 0x00FF) {
                case 0x0007: {
                    int x = (opcode & 0x0F00) >> 8;
                    V[x] = (char) delay_timer;
                    pc += 2;
                    // printf("Set V[x] to delay_timer = %d\n", delay_timer);
                    break;
                }

                case 0x000A: {
                    int x = (opcode & 0x0F00) >> 8;

                    for(int i = 0; i < 16; i++) {
                        if (getKeyBuffer()[i] == 1) {
                            V[x] = i;
                            pc += 2;
                            //printf("Key pressed\n");
                            break;
                        }
                    }
                    break;
                }

                case 0x0015: {
                    int x = (opcode & 0x0F00) >> 8;
                    delay_timer = V[x];
                    pc += 2;
                    // printf("Set delay_timer to V[%x] = %d\n", x, V[x]);
                    break;
                }

                case 0x0018: {
                    int x = (opcode & 0x0F00) >> 8;
                    sound_timer = V[x];
                    pc += 2;
                    // printf("Set sound_timer");
                    break;
                }

                case 0x001E: {
                    int x = (opcode & 0x0F00) >> 8;
                    I += V[x];
                    pc += 2;
                    //printf("Added V[%d] = %d to I\n", x,  V[x]);
                    break;
                }

                case 0x0029: {
                    int x = (opcode & 0x0F00) >> 8;
                    int character = V[x];
                    I = (0x050 + (character * 5));
                    // printf("Setting I to Character V[x] truc\n");
                    pc += 2;
                    break;
                }

                case 0x0033: { // Store binary VX in I, I+1 and I+2
                    int x = (opcode & 0x0F00) >> 8;
                    int value = V[x];
                    int hundreds = (value - (value % 100)) / 100;
                    value -= hundreds * 100;
                    int tens = (value - (value % 10))/ 10;
                    value -= tens * 10;
                    memory[I] = (char)hundreds;
                    memory[I + 1] = (char)tens;
                    memory[I + 2] = (char)value;
                    pc += 2;
                    // printf("Storing Binary-Coded Decimal / pc : %x\n", pc);
                    break;
                }

                case 0x0055: {
                    int x = (opcode & 0x0F00) >> 8;
                    
                    for (int i = 0; i <= x; i++) {
                        memory[I + i] = V[i];
                    }
                    
                    // printf
                    I = (char) (I + x + 1);
                    pc += 2;
                    break;
                }

                case 0x0065: {
                    int x = (opcode & 0x0F00) >> 8;

                    for (int i = 0; i <= x; i++) {
                        V[i] = memory[I + i];
                    }

                    // printf("Setting V[0] to V[x] to the values of memory\n");
                    I = (char) (I + x + 1);
                    pc += 2;
                    break;
                }

                default:
                    fprintf(stderr, "%x: Opcode not implemented.\n", opcode);
                    stop();
                    break;
            }
            break;

        default:
            fprintf(stderr, "%x: Opcode not implemented.\n", opcode);
            stop();
            break;
    }

    if (sound_timer > 0)
        sound_timer--;
    if (delay_timer > 0)
        delay_timer--;
}

int needsRedraw() {
    return redraw;
}

void removeDrawFlag() {
    redraw = 0;
}

unsigned char *getDisplay() {
    return display;
}

void loadFontset() {
    for (int i = 0; i < sizeof(fontset)/sizeof(fontset[0]); i++) {
        memory[0x50 + i] = fontset[i] & 0xFF;
    }
}

void loadProgram(char *fileName) {
    FILE *file;

    file = fopen(fileName,"rb");  // r for read, b for binary

    int c = 0;
    int offset = 0;
    while ((c=fgetc(file)) != EOF) {
        memory[0x200 + offset] = c;
        offset++;
    }

    fclose(file);
}

void printMemory() {
    for (int i = 0; i < sizeof(memory)/sizeof(memory[0]); i++) {
        if (memory[i] < 16) {
            printf("0%x ", memory[i]);
        } else {
            printf("%x ", memory[i]);
        }
        if ((i+1) % 16 == 0) {
            printf("\n");
        }
    }
}